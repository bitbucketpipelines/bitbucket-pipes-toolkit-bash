# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.6.0

- minor: Add check_for_newer_version function to check pipe's version.

## 0.5.0

- minor: Added support for array variables

## 0.4.0

- minor: Add capture stdout to output_file

## 0.3.0

- minor: Don't capture the output of the run command

## 0.2.0

- minor: run now sets the output variable as well

## 0.1.1

- patch: Fix the issue with /dev/stdout not available

## 0.1.0

- minor: Initial version bump

